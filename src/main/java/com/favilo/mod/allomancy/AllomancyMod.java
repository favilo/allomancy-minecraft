package com.favilo.mod.allomancy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.favilo.mod.allomancy.events.AllomancyEventHandler;
import com.favilo.mod.allomancy.events.PewterEventHandler;
import com.favilo.mod.allomancy.items.Potions;
import com.favilo.mod.allomancy.network.PacketDispatcher;
import com.favilo.mod.allomancy.proxy.CommonProxy;


@Mod(modid = Constants.MODID, version = Constants.VERSION, name = Constants.NAME)
public class AllomancyMod {
  @SidedProxy(clientSide = Constants.CLIENT_PROXY_CLASS, serverSide = Constants.SERVER_PROXY_CLASS)
  public static CommonProxy proxy;
  
  @Instance(Constants.MODID)
  public static AllomancyMod instance;
  
  @EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    PacketDispatcher.registerPackets();
    registerHandlers();
  }

  private void registerHandlers() {
    MinecraftForge.EVENT_BUS.register(new AllomancyEventHandler());
    MinecraftForge.EVENT_BUS.register(new PewterEventHandler());
  }

  @EventHandler
  public void init(FMLInitializationEvent event) {
    Potions.init();
    proxy.registerModels();
  }

  @EventHandler
  public void postInit(FMLPostInitializationEvent event) {
  }
}
