package com.favilo.mod.allomancy;

public class Constants {
  public static final String MODID = "allomancy";
  public static final String VERSION = "0.0.1";
  public static final String NAME = "Allomancy";
  public static final String CLIENT_PROXY_CLASS = "com.favilo.mod.allomancy.proxy.ClientProxy";
  public static final String SERVER_PROXY_CLASS = "com.favilo.mod.allomancy.proxy.CommonProxy";
}
