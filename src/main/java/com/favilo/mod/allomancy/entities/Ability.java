package com.favilo.mod.allomancy.entities;

import com.favilo.mod.allomancy.network.PacketDispatcher;
import com.favilo.mod.allomancy.network.packet.client.SyncAbilityPropsMessage;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class Ability {

  public static final int MAX_METAL_STORAGE = 0xffff;

  public final EntityPlayer player;
  private final Metal metal;
  private int current;
  private double strength;

  private Ability(EntityPlayer player, Metal name, int current, double strength) {
    this.player = player;
    this.metal = name;
    this.current = current;
    this.strength = strength;
  }

  Ability(EntityPlayer player, Metal name) {
    this(player, name, 0, 1.0);
  }

  public Metal getMetal() {
    return metal;
  }

  public void setCurrent(int current) {
    this.current = current;
  }

  public boolean modifyCurrent(int delta) {
    int old = current;
    this.current += delta;
    if (current < 0) {
      current = 0;
    } else if (current > MAX_METAL_STORAGE) {
      current = MAX_METAL_STORAGE;
    }
    return old > 0;
  }

  public int getCurrent() {
    return this.current;
  }

  public double getStrength() {
    return strength;
  }

  public void setStrength(double strength) {
    this.strength = strength;
  }

  public String toString() {
    StringBuilder string = new StringBuilder(metal.toString());
    string.append(": (").append(current).append(", ").append(strength).append(")");
    return string.toString();
  }

  public boolean isBurning() {
    return strength > 1.0 && current > 0;
  }

  public void saveNBTData(NBTTagCompound tag) {
    tag.setString("metal", getMetal().name());
    tag.setInteger("current", getCurrent());
    tag.setDouble("strength", getStrength());
  }

  public void loadNBTData(NBTTagCompound tag) {
    String tagName = tag.getString("metal");
    if (!tagName.equals(getMetal().name())) {
      throw new IllegalArgumentException(
          "Attempting to load Ability with wrong Compound: " + tagName + "=/=" + getMetal().name());
    }
    current = tag.getInteger("current");
    strength = tag.getDouble("strength");
  }

  public void syncToClient() {
    if (FMLCommonHandler.instance().getEffectiveSide().isServer()) {
      PacketDispatcher.sendTo(new SyncAbilityPropsMessage(this), (EntityPlayerMP) player);
    }
  }
}
