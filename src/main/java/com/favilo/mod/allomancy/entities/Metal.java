package com.favilo.mod.allomancy.entities;

import java.awt.Color;

import net.minecraft.item.ItemStack;

public enum Metal {
  // Internal Physical
  PEWTER(new Color(157, 154, 150)), // Tin 91%/Lead 9%
  TIN(new Color(211, 212, 213)), // Pure metal

  // External Physical
  STEEL(new Color(224, 223, 219)), // Iron and Carbon (charcoal/coal)
  IRON(new Color(67, 75, 77)), // Pure metal

  // Internal Mental
  COPPER(new Color(184, 115, 51)), // Pure metal
  BRONZE(new Color(205, 127, 50)), // Copper and Tin

  // External Mental
  BRASS(new Color(181, 166, 66)), // Zinc and Copper
  ZINC(new Color(186, 196, 200)); // Pure metal

  private final Color color;

  Metal(Color color) {
    this.color = color;
  }

  public String toString() {
    return name().toLowerCase();
  }

  public static Metal fromMetadata(int meta) {
    return values()[meta];
  }

  public static Metal fromStack(ItemStack stack) {
    int metadata = stack.getMetadata() < 0 ? 0 : stack.getMetadata();
    return fromMetadata(metadata);
  }

  public int getColorRGB() {
    return color.getRGB();
  }
}
