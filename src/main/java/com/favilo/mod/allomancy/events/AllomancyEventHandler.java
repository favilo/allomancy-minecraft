package com.favilo.mod.allomancy.events;

import com.favilo.mod.allomancy.AllomancyMod;
import com.favilo.mod.allomancy.entities.Ability;
import com.favilo.mod.allomancy.entities.Metal;
import com.favilo.mod.allomancy.entities.MetalbornPlayer;
import com.favilo.mod.allomancy.network.PacketDispatcher;
import com.favilo.mod.allomancy.network.packet.client.SyncPlayerPropsMessage;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;

public class AllomancyEventHandler {

  @SubscribeEvent
  public void onEntityConstructing(EntityConstructing event) {
    if (event.entity instanceof EntityPlayer
        && MetalbornPlayer.get(event.entity) == null) {
      EntityPlayer player = (EntityPlayer) event.entity;
      MetalbornPlayer.register(player).makeMistborn();
    }
  }
  
  @SubscribeEvent
  public void onEntityJoinWorld(EntityJoinWorldEvent event) {
    if (!event.entity.worldObj.isRemote && event.entity instanceof EntityPlayer) {
      MetalbornPlayer.get((EntityPlayer)event.entity).syncToClient();
    }
  }
  
  @SubscribeEvent
  public void onClonePlayer(PlayerEvent.Clone event) {
    NBTTagCompound compound = new NBTTagCompound();
    MetalbornPlayer.get(event.original).saveNBTData(compound);
    MetalbornPlayer.get(event.entityPlayer).loadNBTData(compound);
  }
}
