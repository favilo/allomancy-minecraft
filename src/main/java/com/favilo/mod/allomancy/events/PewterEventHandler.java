package com.favilo.mod.allomancy.events;

import com.favilo.mod.allomancy.entities.Ability;
import com.favilo.mod.allomancy.entities.Metal;
import com.favilo.mod.allomancy.entities.MetalbornPlayer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Event Handler for Pewter.
 * 
 * This will subscribe and listen to events that are important to people burning pewter.
 * @author favilo
 */
public class PewterEventHandler {
  /**
   * Handler when the allomancer falls.
   */
  @SubscribeEvent
  public void onLivingFallEvent(LivingFallEvent event) {
    MetalbornPlayer props = MetalbornPlayer.get(event.entity);
    if (props == null) {
      return;
    }
    
    Ability pewter = props.getMetal(Metal.PEWTER);
    if (pewter.isBurning()) {
      if (event.distance < 5) {
        // Negate fall damage for falls less than 5
        event.distance = 0;
      }
    }
  }
  
  /**
   * Handler for when the allomancer gets hurt.
   */
  @SubscribeEvent
  public void onLivingHurtEvent(LivingHurtEvent event) {
    MetalbornPlayer props = MetalbornPlayer.get(event.entity);
    if (props == null) {
      return;
    }
    
//    Minecraft.getMinecraft().setIngameNotInFocus();
    Ability pewter = props.getMetal(Metal.PEWTER);
    if (pewter.isBurning()) {
      event.ammount = (float) (event.ammount / pewter.getStrength());
    }
  }
  
  /**
   * Handler for when the allomancer jumps.
   */
  @SubscribeEvent
  public void onLivingJump(LivingJumpEvent event) {
    MetalbornPlayer props = MetalbornPlayer.get(event.entity);
    if (props == null) {
      return;
    }
    
    Ability pewter = props.getMetal(Metal.PEWTER);
    if (pewter.isBurning()) {
      EntityPlayer player = (EntityPlayer)event.entity;
      player.motionY *= pewter.getStrength() * 0.167;
    }
  }
  
  /**
   * Tick updater. I think I may use this to have the passive burn of metals.
   * 
   * But there might be a better way to accomplish the same thing.
   */
  @SubscribeEvent
  public void onLivingUpdate(LivingUpdateEvent event) {
    MetalbornPlayer props = MetalbornPlayer.get(event.entity);
    if (props == null) {
      return;
    }
  }
}
