package com.favilo.mod.allomancy.items;

import java.awt.Color;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import com.favilo.mod.allomancy.AllomancyMod;
import com.favilo.mod.allomancy.Constants;
import com.favilo.mod.allomancy.entities.Metal;
import com.favilo.mod.allomancy.entities.MetalbornPlayer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry.ItemStackHolder;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MetalVial extends ItemPotion {

  public MetalVial() {
    setMaxStackSize(64);
    setHasSubtypes(true);
    setUnlocalizedName("metalvial");
    setCreativeTab(CreativeTabs.tabMisc);
  }

  @Override
  public int getColorFromItemStack(ItemStack stack, int renderPass) {
    switch (renderPass) {
      case 0:
        return Metal.fromStack(stack).getColorRGB();
      case 1:
        return Color.LIGHT_GRAY.getRGB();
      default:
        return Color.BLACK.getRGB();
    }
  };

  @Override
  public int getColorFromDamage(int meta) {
    return Metal.fromMetadata(meta).getColorRGB();
  }

  @SideOnly(Side.CLIENT)
  @Override
  public boolean isEffectInstant(int meta) {
    return true;
  }

  @Override
  public List<PotionEffect> getEffects(ItemStack stack) {
    return getEffects(stack.getMetadata());
  };

  @Override
  public List<PotionEffect> getEffects(int metadata) {
    return ImmutableList.<PotionEffect>of();
  }

  @Override
  public void addInformation(ItemStack stack, EntityPlayer player, List tooltip, boolean advanced) {
    Metal m = Metal.fromStack(stack);
    tooltip.add(StatCollector.translateToLocal("item." + m + "_vial.effect"));
  };

  @Override
  public void getSubItems(Item item, CreativeTabs tab, java.util.List subItems) {
    for (Metal m : Metal.values()) {
      subItems.add(new ItemStack(item, 1, m.ordinal()));
    }
  };

  @Override
  public ItemStack onItemUseFinish(ItemStack stack, World world, EntityPlayer player) {
    if (!player.capabilities.isCreativeMode) {
      --stack.stackSize;
    }

    Metal metal = Metal.fromStack(stack);

    if (!world.isRemote) {
      System.err.printf("[Allomancy]: %s used!\n", metal);
      metalAction(player, metal);
    }

    if (!player.capabilities.isCreativeMode) {
      if (stack.stackSize <= 0) {
        return new ItemStack(Items.glass_bottle);
      }

      player.inventory.addItemStackToInventory(new ItemStack(Items.glass_bottle));
    }

    return stack;
  }

  @Override
  public int getMaxItemUseDuration(ItemStack stack) {
    return 1;
  }

  @Override
  public String getItemStackDisplayName(ItemStack stack) {
    Metal m = Metal.fromStack(stack);
    return StatCollector.translateToLocal("item." + m + "_vial.name");
  }

  private static void metalAction(EntityPlayer p, Metal metal) {
    MetalbornPlayer properties = MetalbornPlayer.get(p);

    properties.addMetal(metal);
  }
}
