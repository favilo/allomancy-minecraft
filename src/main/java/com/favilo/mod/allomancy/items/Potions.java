package com.favilo.mod.allomancy.items;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import net.minecraft.item.ItemPotion;
import net.minecraft.potion.Potion;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class Potions {
  public static ItemPotion metalVial;
  
  public static synchronized void init() {
    metalVial = new MetalVial();
    
    GameRegistry.registerItem(metalVial, metalVial.getUnlocalizedName());
  }
}
