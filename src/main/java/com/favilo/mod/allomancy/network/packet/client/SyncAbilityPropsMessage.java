package com.favilo.mod.allomancy.network.packet.client;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.favilo.mod.allomancy.entities.Ability;
import com.favilo.mod.allomancy.entities.Metal;
import com.favilo.mod.allomancy.entities.MetalbornPlayer;
import com.favilo.mod.allomancy.network.packet.AbstractMessage.AbstractClientMessage;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.relauncher.Side;

public class SyncAbilityPropsMessage extends AbstractClientMessage<SyncAbilityPropsMessage> {
  private static final Logger logger =
      LogManager.getFormatterLogger(SyncAbilityPropsMessage.class);
  
  private NBTTagCompound data;
  private Metal metal;
  
  public SyncAbilityPropsMessage() {}
  
  public SyncAbilityPropsMessage(Ability ability) {
    data = new NBTTagCompound();
    ability.saveNBTData(data);
    metal = ability.getMetal();
  }
  
  @Override
  protected void read(PacketBuffer buffer) throws IOException {
    data = buffer.readNBTTagCompoundFromBuffer();
    metal = Metal.valueOf(data.getString("metal"));
  }

  @Override
  protected void write(PacketBuffer buffer) throws IOException {
    buffer.writeNBTTagCompoundToBuffer(data);
  }

  @Override
  public void process(EntityPlayer player, Side side) {
    logger.info("Syncing " + metal.name() + " to CLIENT");
    MetalbornPlayer.get(player).getMetal(metal).loadNBTData(data);
  }

}
