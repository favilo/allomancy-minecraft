package com.favilo.mod.allomancy.proxy;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.GameRegistry;


public class CommonProxy {
  
  private static final Logger logger = LogManager.getFormatterLogger(CommonProxy.class);
  private static final Map<String, NBTTagCompound> extendedEntityData = new HashMap<>();

  public void registerModels() {}

  public EntityPlayer getPlayerEntity(MessageContext ctx) {
    logger.info("Retrieving player from CommonProxy for message on side " + ctx.side);
    return ctx.getServerHandler().playerEntity;
  }

  public IThreadListener getThreadFromContext(MessageContext ctx) {
    return ctx.getServerHandler().playerEntity.getServerForPlayer();
  }
}
